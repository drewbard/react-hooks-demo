import * as React from "react";
import styled from "styled-components";
import {useGet} from "./hooks-rest-api/index";

interface IPost {
    id: string;
    title: string;
    body: string;
}

const StyledArticle = styled.article`
  height: 250px;
  width: calc(100vw - 2em);
  border: 1px solid gray;
  margin: 2px;
  padding: 10px;
  overflow: hidden;
  @media(min-width: 768px) {
  width: 250px;
  }
`;

const Post: React.SFC<IPost> = ({title, body}) => (
    <StyledArticle>
        <h5>{title}</h5>
        <p>{body}</p>
    </StyledArticle>
);

const PostsLayout = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  flex-wrap: wrap;
  @media(min-width: 768px) {
    justify-content: center;
    flex-direction: row;
  }
`;

const Posts: React.SFC = () => {
    const {error, loading, data = [] } = useGet<IPost[]>("/posts", []);

    if (loading) { return "...Loading"; }
    if (error) { return `Some error: ${data}!!!`; }

    return (
        <PostsLayout>
            {data.map((post) => (
                <Post key={post.id}
                      title={post.title}
                      body={post.body}
                />
            ))}
        </PostsLayout>

    );
};

export default Posts;
