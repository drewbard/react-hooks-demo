import * as React from "react";
import styled from "styled-components";
import Posts from "./Posts";

const Layout = styled.div`
  width: 100vw;
`;

const App = () => (
    <Layout>
        <h1>Posts</h1>
        <Posts/>
    </Layout>
);

export default App;
