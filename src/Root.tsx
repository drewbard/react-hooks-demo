import * as React from "react";
import App from "./App";
import {ApiProvider, creatApi} from "./hooks-rest-api/index";

const api = creatApi({baseURL: "https://jsonplaceholder.typicode.com"});

const Root = () => (
    <ApiProvider value={api}>
        <App/>
    </ApiProvider>
);

export default Root;
