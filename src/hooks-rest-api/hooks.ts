import {useContext, useEffect, useReducer} from "react";
import {ApiContext} from "./context";
import {failed, IAction, IState, load, reducer, success} from "./reducer";

export const useGet = <T>(path: string, initialState: T): IState<T> => {
    const [state, dispatch] = useReducer<IState<T>, IAction<T>>(reducer, initialState);

    const api = useContext(ApiContext);

    useEffect(function loadData() {
        if (state.loading) { return; }
        if (state.error) { return; }
        if (!state.loading && state.data) { return; }

        dispatch(load());

        api
            .get<T>(path)
            .then(({data}) => data)
            .then((data) => dispatch(success(data)))
            .catch((error) => dispatch(failed(error)));
    });

    return state;
};
