// Action Types
const LOAD = "LOAD";
type LOAD = typeof LOAD;

const SUCCESS = "SUCCESS";
type SUCCESS = typeof SUCCESS;

const FAILED = "FAILED";
type FAILED = typeof FAILED;

// Action Creators
export interface IAction<T> {
    type: FAILED | LOAD | SUCCESS;
    payload?: T | Error;
    error?: boolean;
}

export const load = (): IAction => ({
    type: LOAD,
});

export const success = (payload): IAction => ({
    payload,
    type: SUCCESS,
});

export const failed = (payload): IAction => ({
    error: true,
    payload,
    type: FAILED,
});

// Initial State
export interface IState<T> {
    loading: boolean;
    data: T | Error;
    error: boolean;
}

const initialState: IState = {
    data: null,
    error: null,
    loading: false,
};

export const reducer = <T>(state: IState<T> = initialState, action: IAction<T>) => {
    switch (action.type) {
        case LOAD:
            return {loading: true, error: false, ...state};
        case SUCCESS:
            return {loading: false, error: false, data: action.payload};
        case FAILED:
            return {loading: false, error: true, data: action.payload};
    }
};
