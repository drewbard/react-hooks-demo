import axios, {AxiosInstance} from "axios";
import * as React from "react";

export const creatApi = axios.create;
export const ApiContext = React.createContext<AxiosInstance>(null);
export const ApiProvider = ApiContext.Provider;
