const merge = require('webpack-merge');
const path = require('path');
const common = require('./webpack.common.js');
const CleanWebpackPlugin = require('clean-webpack-plugin');

module.exports = merge(common, {
	mode: 'production',
	plugins: [
		new CleanWebpackPlugin(),
	],
	output: {
		filename: 'bundle.[contenthash].js',
		path: path.resolve(__dirname, 'dist')
	},
});
