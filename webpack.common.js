const HtmlWebPackPlugin = require("html-webpack-plugin");

module.exports = {
	resolve: {
		extensions: ['.ts', '.js', '.jsx', '.tsx', '.json'],
	},
	plugins: [
		new HtmlWebPackPlugin({
			template: "./src/index.html",
			filename: "./index.html"
		}),
	],
	module: {
		rules: [
			{
				test: /\.(js|ts)x?$/,
				exclude: /node_modules/,
				use: {
					loader: "babel-loader",
					
				},
			},
			{
				test: /\.html$/,
				use: [
					{
						loader: "html-loader",
						options: {minimize: true}
					}
				]
			}
		],
	}
};
