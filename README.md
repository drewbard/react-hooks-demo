# react-hooks-demo
Playing around with React hooks and Typescript.

## Hooks Used
- custom
- useEffect
- useState
- useContext
- useReducer
- strongly typed hooks w/ typescript
